import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.consumer.junit.MockServerConfig;
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt;
import au.com.dius.pact.consumer.junit5.PactTestFor;
import au.com.dius.pact.core.model.PactSpecVersion;
import au.com.dius.pact.core.model.RequestResponsePact;
import au.com.dius.pact.core.model.annotations.Pact;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.IOUtils;
import org.apache.hc.client5.http.fluent.Request;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@ExtendWith(PactConsumerTestExt.class)
@PactTestFor(providerName = "PersonProvider", pactVersion = PactSpecVersion.V3)
@MockServerConfig(hostInterface = "localhost", port = "1234")
public class PactFlowTest {
    private final Map<String, String> headers = MapUtils.putAll(new HashMap<>(), new String[]{
            "Content-Type", "application/json"
    });

    @BeforeEach
    public void setUp(MockServer mockServer) {
        assertThat(mockServer, is(notNullValue()));
    }

    @Pact(consumer = "Person")
    public RequestResponsePact person(PactDslWithProvider builder) {
        return builder
                .given("Person exist")
                .uponReceiving("Retrieving data")
                .path("/getPerson")
                .method("GET")
                .willRespondWith()
                .headers(headers)
                .status(200)
                .body("""
                        {
                            "name": "John",
                            "surname": "Doe"
                        }""")
                .toPact();
    }

    @Pact(consumer = "Person")
    public RequestResponsePact personDoesNotExist(PactDslWithProvider builder) {
        return builder
                .given("No person exist")
                .uponReceiving("retrieving person data")
                .path("/getPerson")
                .method("GET")
                .willRespondWith()
                .headers(headers)
                .body("""
                        {
                         "message": "NOT FOUND"
                        }
                        """)
                .status(404)
                .toPact();
    }

    @Test
    @PactTestFor(pactMethod = "person")
    void testPerson(MockServer mockServer) throws IOException {
        ClassicHttpResponse httpResponse = (ClassicHttpResponse) Request.get(mockServer.getUrl() + "/getPerson").execute().returnResponse();
        assertThat(httpResponse.getCode(), is(equalTo(200)));
        assertThat(IOUtils.toString(httpResponse.getEntity().getContent()),
                is(equalTo("""
                        {
                            "name": "John",
                            "surname": "Doe"
                        }""")));
    }

    @Test
    @PactTestFor(pactMethod = "personDoesNotExist")
    void testPersonDoNotExist(MockServer mockServer) throws IOException {
        ClassicHttpResponse httpResponse = (ClassicHttpResponse) Request.get(mockServer.getUrl() + "/getPerson").execute().returnResponse();
        assertThat(httpResponse.getCode(), is(equalTo(404)));
        assertThat(IOUtils.toString(httpResponse.getEntity().getContent()), is(equalTo("""
                        {
                         "message": "NOT FOUND"
                        }
                        """)));
    }
}